package tests;

import calculator.UserInputParser;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;


import static org.junit.jupiter.api.Assertions.assertEquals;

@DisplayName("Custom String Parser Tests:")
public class ParserTest {
    private static UserInputParser parser;

    @BeforeEach
    public void setUp() {
        parser = new UserInputParser("");
    }

    @Nested
    @DisplayName("When parsing equation strings")
    class TestParseInput {
        @ParameterizedTest
        @MethodSource("tests.resources.ParserTestParams#provideStringsParseInput")
        public void testParseInput(String input, String expected) {
            parser = new UserInputParser(input);
            assertEquals(expected, parser.parseInput());
        }
    }

    @Nested
    @DisplayName("When constructing a custom parser")
    class TestCreatingParser {
        @ParameterizedTest
        @MethodSource("tests.resources.ParserTestParams#provideStringsForCreatingParser")
        public void testCreateParser(String input, String expected) {
            parser = new UserInputParser(input);
            assertEquals(expected, parser.getInput());
        }
    }


    @Nested
    @DisplayName("When using the helper function isNumeric()")
    class TestIsNumeric {
        @ParameterizedTest
        @MethodSource("tests.resources.ParserTestParams#provideStringsForIsNumeric")
        public void testIsNumeric(String input, boolean expected) {
            assertEquals(expected, parser.isNumeric(input));
        }
    }

    @Nested
    @DisplayName("When using the helper function isLeftAssociated()")
    class TestIsLeftAssociated {
        @ParameterizedTest
        @MethodSource("tests.resources.ParserTestParams#provideStringsForIsLeftAssociated")
        public void testIsLeftAssociated(String input, boolean expected) {
            assertEquals(expected, parser.isLeftAssociated(input));
        }
    }

    @Nested
    @DisplayName("When using the helper function precedence()")
    class TestPrecedence {
        @ParameterizedTest
        @MethodSource("tests.resources.ParserTestParams#provideStringsForPrecedence")
        public void testPrecedence(String input, int expected) {
            assertEquals(expected, parser.precedence(input));
        }
    }
}


