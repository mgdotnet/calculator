package tests.resources;

import org.junit.jupiter.params.provider.Arguments;

import java.util.stream.Stream;

public class ParserTestParams {
    static Stream<Arguments> provideStringsParseInput() {
        return Stream.of(
                Arguments.of("2 + 2", "2 2 +"),
                Arguments.of("2 + 22", "2 22 +"),
                Arguments.of("2 + 456", "2 456 +"),
                Arguments.of("3 + 44 * 2 / ( 1 - 5 ) ^ 2 ^ 3", "3 44 2 * 1 5 - 2 3 ^ ^ / +"),
                Arguments.of("3.4 + 44 * 2 / ( 1 - 5 ) ^ 2 ^ 3", "3.4 44 2 * 1 5 - 2 3 ^ ^ / +"),
                Arguments.of("", ""),
                Arguments.of(null, "")

        );
    }

    static Stream<Arguments> provideStringsForCreatingParser() {
        return Stream.of(
                Arguments.of("2 + 2", "2+2"),
                Arguments.of("2 + 22", "2+22"),
                Arguments.of("2 + 456", "2+456"),
                Arguments.of("3 + 44 * 2 / ( 1 - 5 ) ^ 2 ^ 3", "3+44*2/(1-5)^2^3"),
                Arguments.of("", ""),
                Arguments.of(null, "")

        );
    }

    static Stream<Arguments> provideStringsForIsNumeric() {
        return Stream.of(
                Arguments.of("1", true),
                Arguments.of("2", true),
                Arguments.of("x", false),
                Arguments.of(null, false),
                Arguments.of("1.2", true),
                Arguments.of("-1", true)
        );
    }

    static Stream<Arguments> provideStringsForIsLeftAssociated() {
        return Stream.of(
                Arguments.of("^", false),
                Arguments.of("*", true),
                Arguments.of("+", true),
                Arguments.of("-", true),
                Arguments.of("/", true),
                Arguments.of(null, false)
        );
    }

    static Stream<Arguments> provideStringsForPrecedence() {
        return Stream.of(
                Arguments.of("^", 3),
                Arguments.of("*", 2),
                Arguments.of("+", 1),
                Arguments.of("-", 1),
                Arguments.of("/", 2),
                Arguments.of(null, -1),
                Arguments.of("something", -1)
        );
    }
}
