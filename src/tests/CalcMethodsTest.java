package tests;

import calculator.CalcMethods;
import calculator.UserInputParser;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

@DisplayName("Calculator Method Tests:")
public class CalcMethodsTest {
    private CalcMethods calc;

    @BeforeEach
    public void setuUp() {
        calc = new CalcMethods();
    }

    /**
     * Using Nested classes we can explain our testing scenarios with
     * DisplayNames
     * */

    @Nested
    @DisplayName("When performing simple addition")
    class SimpleAddition {

        @Test
        @DisplayName("On two positive numbers")
        void testAdditionOnPositives() {
            assertEquals(10, calc.add(5, 5));
        }

        @Test
        @DisplayName("On one positive and one negative")
        void testAdditionOnMix() {
            assertEquals(3, calc.add(5, -2));
        }

        @Test
        @DisplayName("On two negative numbers")
        void testAdditionOnNegatives() {
            assertEquals(-5, calc.add(-3, -2));
        }
    }

/**
 * Simple test case examples
 * */

    @Test
    public void testMinus() {
        assertEquals(5, calc.minus(7, 2));
        assertEquals(-5, calc.minus(2, 7));
        assertEquals(9, calc.minus(2, -7));
    }

    @Test
    public void testMult() {
        assertEquals(100, calc.mult(10, 10));
        assertEquals(-10, calc.mult(10, -1));
        assertEquals(10, calc.mult(-10, -1));
        assertEquals(0, calc.mult(10, 0));
    }

    @Test
    public void testDivide() {
        assertEquals(3, calc.divide(6, 2));
        assertThrows(ArithmeticException.class, () -> {
            calc.divide(10, 0);
        });
    }

    @Test
    public void testSqrt() {
        assertEquals(2, calc.sqrt(4));
    }

    @Test
    public void testPower() {
        assertEquals(100, calc.pow(10, 2));
    }

    @Test
    public void testCalculatePostfixEquation() {
        assertEquals(4, calc.calculatePostfixEquation("2 2 +"));
        assertEquals(458, calc.calculatePostfixEquation("2 456 +"));
    }

    @Test
    public void testParseAndCalculate() {
        UserInputParser parser = new UserInputParser("3 * 9 - (6 +1) + 2 ^ 3");
        String postfix = parser.parseInput();

        assertEquals(28, calc.calculatePostfixEquation(postfix));
    }
}
