package tests;

import calculator.CalcDriver;
import calculator.CalcMethods;
import calculator.UserInputParser;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;


@DisplayName("Calculator Driver Tests:")
public class CalcDriverTest {
    private CalcDriver driver;

    @BeforeEach
    public void setuUp() {
    	driver = new CalcDriver();
    }

    /**
     * Using Nested classes we can explain our testing scenarios with
     * DisplayNames
     * */
    @Nested
    @DisplayName("Read Input From User (limit input)")
    class LimitInput {

        @Test
        @DisplayName("Empty call")
        void testUserGivesNoInput() {
            assertEquals("", CalcDriver.readInputFromUser(""));
        }
        
        @Test
        @DisplayName("Basic correct input returns correct string")
        void basicCorrectInput() {
        	assertEquals("1", CalcDriver.readInputFromUser("1"));
        	assertEquals("1+2", CalcDriver.readInputFromUser("1+2"));
        	assertEquals("27-10", CalcDriver.readInputFromUser("27-10"));
        	assertEquals("2*3", CalcDriver.readInputFromUser("2*3"));
        	assertEquals("4/2", CalcDriver.readInputFromUser("4/2"));
        	assertEquals("2^8", CalcDriver.readInputFromUser("2^8"));   
        	assertEquals("-1+-29", CalcDriver.readInputFromUser("-1+-29")); 
        }
        
        @Test
        @DisplayName("Complicated correct input returns correct string")
        void ComplicatedCorrectInput() {
        	assertEquals("3 * 9 - (6 +1) + 2 ^ 3", CalcDriver.readInputFromUser("3 * 9 - (6 +1) + 2 ^ 3"));
        	assertEquals("3*9-(6+1)+2^3", CalcDriver.readInputFromUser("3*9-(6+1)+2^3"));
        	assertEquals("-3*9-(-6+1)+2^-3", CalcDriver.readInputFromUser("-3*9-(-6+1)+2^-3"));
        }
        @Test
        @DisplayName("Verify Bad user input is ignored")
        void BadInputIgnored() {
        	assertEquals("", CalcDriver.readInputFromUser("abcdefghijklmnoprstuvwxyz"));
        	assertEquals("", CalcDriver.readInputFromUser("1+2(3-4)z"));
        	assertEquals("1+2(3-4)", CalcDriver.readInputFromUser("1+2(3-4)"));
        }
    }
}
