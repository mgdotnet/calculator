package calculator;

import java.util.LinkedList;
import java.util.Queue;
import java.util.Stack;

public class UserInputParser {

    private String input;
    private StringBuilder postfix;
    private Queue<String> outputQueue;
    private Stack<String> operator;

    public UserInputParser(String input) {
        if (input == null) {
            this.input = "";
        } else {
            this.input = input.replaceAll(" ", "");
        }
    }

    public String getInput() {
        return this.input;
    }

    /**
     * Parse input is going to run through the user input and convert it from infix to postfix
     * This will allow us to handle order of operations much easier
     * */
    public String parseInput() {
        this.postfix = new StringBuilder();
        this.operator = new Stack<>();
        this.outputQueue = new LinkedList<>();

        String value = "";
        for (int i = 0; i < this.input.length(); i++) {
            value += String.valueOf(this.input.charAt(i));

            if (isNumeric(value) && (i+1) != this.input.length() && isNumeric(String.valueOf(this.input.charAt(i + 1)))) {
                continue;
            }

            if (value.equals("")) {
                value += String.valueOf(this.input.charAt(i));
            }

            // At this point our value is ready for the infix to postfix algorithm
            // credit https://en.wikipedia.org/wiki/Shunting-yard_algorithm
            if (isNumeric(value)) {
                this.outputQueue.add(value);

            } else {
                if (operator.isEmpty()) {
                    operator.push(value);
                } else if (!operator.peek().equals("(")) {
                    // If incoming operator has greater precedence than just pop to stack
                    if (precedence(value) > precedence(operator.peek())) {
                        this.operator.push(value);

                    } else if (value.equals("(")) {
                        this.operator.push(value);
                    } else if (value.equals(")")) {
                        while (!this.operator.peek().equals("(")) {
                            this.outputQueue.add(this.operator.pop());
                        }
                        // Remove the left paren from stack
                        this.operator.pop();
                    } else if ((isLeftAssociated(value) && (precedence(value) <= precedence(operator.peek()))) ||
                                !isLeftAssociated(value) && precedence(value) < precedence(operator.peek())) {
                        this.outputQueue.add(this.operator.pop());
                        this.operator.push(value);
                    } else if (precedence(value) == precedence(operator.peek()) && !isLeftAssociated(value)) {
                        this.operator.push(value);
                    }
                } else {
                    this.operator.push(value);
                }

            }
            value = "";
        }

        // Pop stacks and create postfix string
        while (!this.operator.isEmpty()) {
            this.outputQueue.add(this.operator.pop());
        }

        while (!this.outputQueue.isEmpty()) {
            this.postfix.append(this.outputQueue.poll()).append(" ");
        }

        return this.getPostfix();
    }

    public int precedence(String operator){
        if (operator == null) {
            return -1;
        }
        switch (operator){
            case "+":
            case "-":
                return 1;
            case "*":
            case "/":
                return 2;
            case "^":
                return 3;
        }
        return -1;
    }

    public boolean isLeftAssociated(String operator) {
        if (operator == null) {
            return false;
        }
        switch (operator) {
            case "^":
                return false;
        }
        return true;
    }

    public boolean isNumeric(String s) {
        if (s == null) {
            return false;
        }

        if (s.equals(".")) {
            return true;
        }

        try {
            Double.parseDouble(s);
        } catch (NumberFormatException nfe) {
            return false;
        }

        return true;
    }

    public String getPostfix() {
        return this.postfix.toString().trim();
    }
}
