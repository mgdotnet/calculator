package calculator;

import java.util.ArrayList;
import java.util.Stack;

public class CalcMethods {

    public CalcMethods(){}

    public static double add(double a, double b) {
        return a + b;
    }

    public static double minus(double a, double b) {
        return a - b;
    }

    public static double mult(double a, double b) {
        return a * b;
    }

    public static double divide(double a, double b) throws ArithmeticException {
        // is b he bottom number?
        if (b == 0.0) {
            throw new ArithmeticException("Can't divide by zero");
        }
        return a / b;
    }

    public static double sqrt(double a) {
        return Math.sqrt(a);
    }

    public static double pow(double a, int pow ){
        double ans = 0.0;
        for (int i = 1; i < pow; i++) {
            ans += a * a;
        }
        return ans;
    }

    public static double calculatePostfixEquation(String equation) {
        Stack<Double> stack = new Stack<>();
        String[] parts = equation.split(" ");

        for (int i = 0; i < parts.length; i++) {
            double x, y;
            switch (parts[i]) {
                case "+":
                    stack.push(add(stack.pop(), stack.pop()));
                    break;
                case "-":
                    y = stack.pop();
                    x = stack.pop();
                    stack.push(minus(x, y));
                    break;
                case "/":
                    y = stack.pop();
                    x = stack.pop();
                    stack.push(divide(x, y));
                    break;
                case "*":
                    stack.push(mult(stack.pop(), stack.pop()));
                    break;
                case "^":
                    y = stack.pop().doubleValue();
                    x = stack.pop();
                    stack.push(pow(x, (int) y));
                    break;
                default:
                    stack.push(Double.parseDouble(parts[i]));
            }
        }

        return stack.pop();
    }

}
