package calculator;

import java.util.Scanner;

public class CalcDriver {
	private static boolean isExiting = false;
	private static Scanner sc = new Scanner(System.in);
	
    public static void main(String[] args)throws java.lang.Exception {
    	
    	System.out.println("Welcome to the calculator.  ");
    	System.out.println("Please enter an equation in the form a operator b operator c followed by enter.  ");
    	System.out.println("Press 'Q' or 'q' to exit when finished.");
    	
    	mainControlLoop();        
    }
    
    public static String readInputFromUser(String input)
    {
    	int pos = 0;
    	StringBuilder builder = new StringBuilder();
    	while(pos < input.length())
    	{
	    	char nextChar =input.charAt(pos);
			switch(nextChar)
			{
	    		case 'q': case 'Q':
	    			System.out.println("Thank you for using the calculator. Exiting Program.");
	    			isExiting = true;
	    			return "";
				case '0': case'1': case'2': case '3': case '4': case '5': case '6': case '7': case'8': case '9':
				case '+': case '-': case '*': case '/': case '^': case '(': case ')': case ' ':
					builder.append(nextChar);
					break;
				default:
					System.out.println("Bad input detected. Please re-enter the equation.");
					return "";
			}
			pos++;
    	}
    	return builder.toString();
    }
	
	public static void processAndCalculate(String input)
	{
		// process request.
		UserInputParser parser = new UserInputParser(input);
    	String postfix = parser.parseInput();
    	System.out.println(CalcMethods.calculatePostfixEquation(postfix));
	}
    
    public static void mainControlLoop() {
    	String limitedInput;
    	while (true)
    	{
    		limitedInput = readInputFromUser(sc.nextLine());
    		if (isExiting)
    		{
    			return;
    		}
    		
			if (limitedInput.length() > 0)
			{
				processAndCalculate(limitedInput);
			}
    	}
    }
}
